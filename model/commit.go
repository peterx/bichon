// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"

	udiff "gitlab.com/bichon-project/bichon/diff"
)

type Commit struct {
	Hash      string    `json:"hash"`
	Title     string    `json:"title"`
	Author    User      `json:"author"`
	Committer User      `json:"committer"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
	Message   string    `json:"message"`
	Diffs     []Diff    `json:"diffs"`

	Metadata CommitMetadata `json:"bichonMetadata"`
}

type CommitMetadata struct {
	Partial bool `json:"partial"`
}

type Diff struct {
	Content     string `json:"content"`
	NewFile     string `json:"newFile"`
	OldFile     string `json:"oldFile"`
	NewMode     string `json:"newMode"`
	OldMode     string `json:"oldMode"`
	CreatedFile bool   `json:"createdFile"`
	RenamedFile bool   `json:"renamedFile"`
	DeletedFile bool   `json:"deletedFile"`
}

func (commit *Commit) Age() string {
	age := time.Since(commit.CreatedAt)
	return formatDuration(age)
}

func (commit *Commit) Activity() string {
	age := time.Since(commit.UpdatedAt)
	return formatDuration(age)
}

func (commit *Commit) GetFile(filename string) *Diff {
	for idx, _ := range commit.Diffs {
		diff := &commit.Diffs[idx]
		if diff.NewFile == filename {
			return diff
		}
	}

	return nil
}

type DiffLineLocation int

const (
	DIFF_LINE_LOCATION_OUTSIDE DiffLineLocation = iota
	DIFF_LINE_LOCATION_CONTEXT
	DIFF_LINE_LOCATION_ADDED
	DIFF_LINE_LOCATION_REMOVED
)

func (diff *Diff) IsBinarySummary() bool {
	return udiff.IsBinaryDiffSummary(diff.Content)
}

// If @line is not present in diff, return (OUTSIDE, updated line)
// If @line is context in diff, return (CONTEXT, updated line)
// If @line is added in diff, return (ADDED, unchanged line)
// @line cannot be deleted in diff
func (diff *Diff) Revert(line uint) (DiffLineLocation, uint, error) {
	hunks, err := udiff.ParseUnifiedDiffHunks(diff.Content)
	if err != nil {
		return DIFF_LINE_LOCATION_OUTSIDE, line, err
	}

	newLine := line
	for idx, hunk := range hunks {
		log.Infof("Hunk old %d:%d new %d:%d", hunk.OldLine, hunk.OldCount, hunk.NewLine, hunk.NewCount)
		if line >= hunk.NewLine {
			distance := line - hunk.NewLine
			if distance < hunk.NewCount {
				log.Infof("Line %d inside hunk %d distance %d", line, idx, distance)
				oldLine := hunk.OldLine
				for _, hunkline := range hunk.Lines {
					log.Infof("Procss dist=%d type=%d %s", distance, hunkline.Type, hunkline.Text)
					if distance == 0 {
						if hunkline.Type == udiff.DIFF_LINE_CONTEXT {
							log.Infof("Rebased line %d to %d (context)", line, newLine)
							return DIFF_LINE_LOCATION_CONTEXT, oldLine, nil
						} else if hunkline.Type == udiff.DIFF_LINE_ADDED {
							log.Infof("Rebased line %d unchanged (added)", line)
							return DIFF_LINE_LOCATION_ADDED, line, nil
						} else {
							//return DIFF_LINE_LOCATION_OUTSIDE, line, fmt.Errorf("Line was unexpectedly in removed hunk")
							continue
						}
					}
					if hunkline.Type == udiff.DIFF_LINE_CONTEXT || hunkline.Type == udiff.DIFF_LINE_ADDED {
						distance--
					}
					if hunkline.Type == udiff.DIFF_LINE_CONTEXT || hunkline.Type == udiff.DIFF_LINE_REMOVED {
						oldLine++
					}
				}

				// This should be unreachable
				log.Errorf("Reached the unreachable")
				return DIFF_LINE_LOCATION_OUTSIDE, line, fmt.Errorf("Line was missing in hunk")
			} else {
				delta := distance - hunk.NewCount
				log.Infof("Line %d after hunk %d delta %d", line, idx, delta)
				newLine = hunk.OldLine + hunk.OldCount + delta
			}
		} else {
			log.Infof("Line %d before hunk %d, stopping", line, idx)
			// before this hunk, we've done far enough now
			break
		}
	}

	log.Infof("Rebased line %d to %d (non-context)", line, newLine)
	return DIFF_LINE_LOCATION_OUTSIDE, newLine, nil
}

// If @line is not present in diff, return (false, updated line)
// If @line is context in diff, return (false, updated line)
// If @line is deleted in diff, return (true, unchanged line)
// @line cannot be added in diff
func (diff *Diff) Apply(line uint) (DiffLineLocation, uint, error) {
	hunks, err := udiff.ParseUnifiedDiffHunks(diff.Content)
	if err != nil {
		return DIFF_LINE_LOCATION_OUTSIDE, line, err
	}

	oldLine := line
	for idx, hunk := range hunks {
		log.Infof("Hunk new %d:%d old %d:%d", hunk.NewLine, hunk.NewCount, hunk.OldLine, hunk.OldCount)
		if line >= hunk.OldLine {
			distance := line - hunk.OldLine
			if distance < hunk.OldCount {
				log.Infof("Line %d inside hunk %d distance %d", line, idx, distance)
				newLine := hunk.NewLine
				for _, hunkline := range hunk.Lines {
					if distance == 0 {
						if hunkline.Type == udiff.DIFF_LINE_CONTEXT {
							log.Infof("Rebased line %d to %d (context) [%s]", line, oldLine, hunkline.Text)
							return DIFF_LINE_LOCATION_CONTEXT, oldLine, nil
						} else if hunkline.Type == udiff.DIFF_LINE_REMOVED {
							log.Infof("Rebased line %d unchanged (removed)", line)
							return DIFF_LINE_LOCATION_REMOVED, line, nil
						} else {
							//	return DIFF_LINE_LOCATION_OUTSIDE, line, fmt.Errorf("Line was unexpectedly in added hunk [%s]", hunkline.Text)
							continue
						}
					}
					if hunkline.Type == udiff.DIFF_LINE_CONTEXT || hunkline.Type == udiff.DIFF_LINE_REMOVED {
						distance--
					}
					if hunkline.Type == udiff.DIFF_LINE_CONTEXT || hunkline.Type == udiff.DIFF_LINE_ADDED {
						newLine++
					}
				}

				// This should be unreachable
				log.Errorf("Reached the unreachable")
				return DIFF_LINE_LOCATION_OUTSIDE, line, fmt.Errorf("Line was missing in hunk")
			} else {
				delta := distance - hunk.OldCount
				log.Infof("Line %d after hunk %d delta %d", line, idx, delta)
				oldLine = hunk.NewLine + hunk.NewCount + delta
			}
		} else {
			log.Infof("Line %d before hunk %d, stopping", line, idx)
			// before this hunk, we've done far enough now
			break
		}
	}

	log.Infof("Rebased line %d to %d (non-context)", line, oldLine)
	return DIFF_LINE_LOCATION_OUTSIDE, oldLine, nil
}
