// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package source

import (
	"time"

	"gitlab.com/bichon-project/bichon/model"
)

type Source interface {
	Ping() error

	GetMergeRequests(onlyOpen bool, updatedAfter *time.Time) ([]model.MergeReq, error)

	GetMergeRequest(mreq *model.MergeReq) (model.MergeReq, error)

	GetVersions(mreq *model.MergeReq) ([]model.Series, error)

	GetPatches(mreq *model.MergeReq, series *model.Series) ([]model.Commit, error)

	GetCommitDiffs(mreq *model.MergeReq, commit *model.Commit) ([]model.Diff, error)

	GetMergeRequestThreads(mreq *model.MergeReq) ([]model.CommentThread, error)

	AddMergeRequestThread(mreq *model.MergeReq, text string, standalone bool, context *model.CommentContext) (model.CommentThread, error)

	AddMergeRequestReply(mreq *model.MergeReq, thread, text string) (model.Comment, error)

	ResolveMergeRequestThread(mreq *model.MergeReq, thread string, resolved bool) error

	AcceptMergeRequest(mreq *model.MergeReq, removeSource bool) error

	ApproveMergeRequest(mreq *model.MergeReq) error

	UnapproveMergeRequest(mreq *model.MergeReq) error
}
