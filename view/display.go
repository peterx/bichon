// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"crypto/tls"
	"fmt"
	"net/url"
	"strings"

	"github.com/gdamore/tcell/v2"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/config"
	"gitlab.com/bichon-project/bichon/controller"
	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/bichon/security"
	"gitlab.com/bichon-project/bichon/view/distractions/pacman"
	"gitlab.com/bichon-project/bichon/view/distractions/snake"
	"gitlab.com/bichon-project/tview"
)

type Display struct {
	*tview.Application

	TokenKey [32]byte
	Config   *config.AppConfig

	Engine    controller.Engine
	MergeReqs model.MergeReqList
	Repos     []model.Repo

	Layout       *tview.Flex
	KeyShortcuts *tview.TextView
	StatusBar    *tview.TextView
	Messages     *MessageBar
	Pages        *tview.Pages
	Index        *IndexPage
	Overview     *OverviewPage
	Detail       *DetailPage
	Projects     *ProjectsPage
	AddProject   *AddProjectPage
	EditProject  *EditProjectPage
	ThisPage     Page

	Snake  *snake.Game
	Pacman *pacman.Game

	FormActive         bool
	ChoosePasswordForm *ChoosePasswordForm
	PasswordForm       *PasswordForm
	SortForm           *SortForm
	FilterForm         *FilterForm
	QuickFilterForm    *QuickFilterForm
	VersionForm        *VersionForm
	ExitConfirmForm    *ConfirmForm

	CtrlC bool
}

func sortNameToOrder(name string) (SortFormOrder, bool) {
	reverse := false
	if strings.HasPrefix(name, "reverse-") {
		name = name[8:]
		reverse = true
	}
	if name == "project-id" {
		return SORT_FORM_ORDER_PROJECT_ID, reverse
	} else if name == "age" {
		return SORT_FORM_ORDER_AGE, reverse
	} else if name == "activity" {
		return SORT_FORM_ORDER_ACTIVITY, reverse
	} else if name == "title" {
		return SORT_FORM_ORDER_TITLE, reverse
	} else if name == "author" {
		return SORT_FORM_ORDER_AUTHOR, reverse
	} else {
		return SORT_FORM_ORDER_AGE, reverse
	}
}

func sortOrderToName(order SortFormOrder, reverse bool) string {
	prefix := ""
	if reverse {
		prefix = "reverse-"
	}

	switch order {
	case SORT_FORM_ORDER_PROJECT_ID:
		return prefix + "project-id"
	case SORT_FORM_ORDER_AGE:
		return prefix + "age"
	case SORT_FORM_ORDER_ACTIVITY:
		return prefix + "activity"
	case SORT_FORM_ORDER_TITLE:
		return prefix + "title"
	case SORT_FORM_ORDER_AUTHOR:
		return prefix + "author"
	}
	return prefix + "project-id"
}

func NewDisplay(tlscfg *tls.Config, httpproxy *url.URL) (*Display, error) {
	err := LoadStyleConfig()
	if err != nil {
		return nil, err
	}

	tview.TabSize = 8
	tview.Styles.PrimitiveBackgroundColor = GetStyleColor(ELEMENT_PRIMITIVE_FILL)
	tview.Styles.ContrastBackgroundColor = GetStyleColor(ELEMENT_CONTRAST_FILL)
	tview.Styles.MoreContrastBackgroundColor = GetStyleColor(ELEMENT_MORE_CONTRAST_FILL)
	tview.Styles.PrimaryTextColor = GetStyleColor(ELEMENT_PRIMARY_TEXT)
	tview.Styles.SecondaryTextColor = GetStyleColor(ELEMENT_SECONDARY_TEXT)
	tview.Styles.TertiaryTextColor = GetStyleColor(ELEMENT_TERTIARY_TEXT)
	tview.Styles.InverseTextColor = GetStyleColor(ELEMENT_INVERSE_TEXT)
	tview.Styles.ContrastSecondaryTextColor = GetStyleColor(ELEMENT_CONTRAST_SECONDARY_TEXT)
	tview.Styles.BorderColor = GetStyleColor(ELEMENT_BORDER)
	tview.Styles.TitleColor = GetStyleColor(ELEMENT_TITLE)
	tview.Styles.GraphicsColor = GetStyleColor(ELEMENT_GRAPHICS)
	tview.DefaultSelectKeys = []tcell.Key{tcell.KeyF10}

	cfg, err := config.LoadAppConfig()
	if err != nil {
		return nil, err
	}

	msgs := NewMessageBar()
	display := &Display{
		Application:  tview.NewApplication(),
		Config:       cfg,
		Layout:       tview.NewFlex(),
		KeyShortcuts: tview.NewTextView().SetDynamicColors(true),
		StatusBar:    tview.NewTextView().SetDynamicColors(true),
		Messages:     msgs,
		Pages:        tview.NewPages(),
	}

	display.Engine = controller.NewEngine(display, tlscfg, httpproxy)

	display.Index = NewIndexPage(display.Application, display)
	display.Overview = NewOverviewPage(display.Application, display)
	display.Detail = NewDetailPage(display.Application, display)
	display.Projects = NewProjectsPage(display.Application, display)
	display.AddProject = NewAddProjectPage(display.Application, display)
	display.EditProject = NewEditProjectPage(display.Application, display)
	display.ChoosePasswordForm = NewChoosePasswordForm(display)
	display.PasswordForm = NewPasswordForm(display)
	order, reverse := sortNameToOrder(cfg.Interface.MergeRequestSort)
	display.SortForm = NewSortForm(display, order, reverse)
	display.FilterForm = NewFilterForm(display)
	display.QuickFilterForm = NewQuickFilterForm(display)
	display.VersionForm = NewVersionForm(display)
	display.ExitConfirmForm = NewConfirmForm("Are you sure you wish to exit Bichon?", display)
	display.Snake = snake.NewGame(display.Application, display)
	display.Pacman = pacman.NewGame(display.Application, display)

	display.SetRoot(display.Layout, true)

	display.Layout.SetDirection(tview.FlexRow).
		AddItem(display.KeyShortcuts, 1, 1, false).
		AddItem(display.Pages, 0, 1, false).
		AddItem(display.StatusBar, 1, 1, false).
		AddItem(display.Messages.Text, 1, 1, false)

	display.Pages.AddPage(display.Index.GetName(), display.Index, true, true)
	display.Pages.AddPage(display.Overview.GetName(), display.Overview, true, true)
	display.Pages.AddPage(display.Detail.GetName(), display.Detail, true, true)
	display.Pages.AddPage(display.Projects.GetName(), display.Projects, true, true)
	display.Pages.AddPage(display.AddProject.GetName(), display.AddProject, true, true)
	display.Pages.AddPage(display.EditProject.GetName(), display.EditProject, true, true)
	display.Pages.AddPage("new-password-form", display.ChoosePasswordForm, true, false)
	display.Pages.AddPage("password-form", display.PasswordForm, true, false)
	display.Pages.AddPage("sort-form", display.SortForm, true, false)
	display.Pages.AddPage("filter-form", display.FilterForm, true, false)
	display.Pages.AddPage("quick-filter-form", display.QuickFilterForm, true, false)
	display.Pages.AddPage("version-form", display.VersionForm, true, false)
	display.Pages.AddPage("exit-confirm-form", display.ExitConfirmForm, true, false)
	display.Pages.AddPage("snake", display.Snake, true, true)
	display.Pages.AddPage("pacman", display.Pacman, true, true)

	display.MergeReqs.Filter = display.FilterForm.GetMergeReqFilter()
	display.MergeReqs.Sorter = display.SortForm.GetMergeReqSorter()

	display.StatusBar.SetText(fmt.Sprintf("[%s:%s]--- Bichon",
		GetStyleColorName(ELEMENT_STATUS_TEXT),
		GetStyleColorName(ELEMENT_STATUS_FILL)) +
		strings.Repeat(" ", 500))

	display.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if display.FormActive {
			return event
		}
		switch event.Key() {
		case tcell.KeyF1:
			display.Pages.SendToFront("snake")
			display.Snake.Play()
			display.Application.SetFocus(display.Snake)
			display.FormActive = true
			return nil

		case tcell.KeyF2:
			display.Pages.SendToFront("pacman")
			display.Pacman.Play()
			display.Application.SetFocus(display.Pacman)
			display.FormActive = true
			return nil
		case tcell.KeyCtrlC:
			if display.CtrlC || display.Config.Interface.DontConfirmExit {
				display.Stop()
			} else {
				display.showForm("exit-confirm-form", display.ExitConfirmForm, true)
				display.CtrlC = true
				return nil
			}
		}
		return display.ThisPage.HandleInput(event)
	})

	display.switchToPage(display.Index)

	if cfg.Projects.TokenMasterKey == config.KeyBackendUndefined {
		if security.KeyringIsAvailable() {
			cfg.Projects.TokenMasterKey = config.KeyBackendKeyring
		} else {
			cfg.Projects.TokenMasterKey = config.KeyBackendArgon2
		}
		err = config.SaveAppConfig(cfg)
		if err != nil {
			log.Infof("Failed to save application config: %s", err)
			return nil, err
		}
	}
	if cfg.Projects.TokenMasterKey == config.KeyBackendArgon2 {
		log.Info("Keyring is not available, falling back to user password")
		if cfg.Projects.Argon2Params == nil {
			display.showForm("new-password-form", display.ChoosePasswordForm, false)
		} else {
			display.showForm("password-form", display.PasswordForm, false)
		}
	} else {
		key, newKey, err := security.EnsureMasterKey()
		if err != nil {
			return nil, err
		}

		display.TokenKey = key

		err = display.loadProjects(newKey)
		if err != nil {
			return nil, err
		}
	}

	return display, nil
}

func (display *Display) loadProjects(compatToken bool) error {
	var err error
	display.Repos, err = config.LoadProjects(display.TokenKey, compatToken)
	if err != nil {
		log.Infof("Failed to load projects: %s", err)
		return err
	}

	if compatToken {
		err = config.SaveProjects(display.Repos, display.TokenKey)
		if err != nil {
			return err
		}
	}

	for _, repo := range display.Repos {
		if repo.State == model.RepoStateActive {
			display.Engine.AddRepository(repo)
		}
	}

	log.Infof("Refreshing views")
	display.Projects.Refresh(display.Application, display.Repos)
	display.FilterForm.Refresh(display.Application, display.Repos)

	if len(display.Repos) == 0 {
		log.Infof("No repos, add new projet")
		display.switchToPage(display.AddProject)
		display.AddProject.LoadLocalProject()
	}

	return nil
}

func (display *Display) showForm(name string, form tview.Primitive, hasEsc bool) {
	display.FormActive = true
	display.Pages.SendToFront(name)
	display.Pages.ShowPage(name)
	display.Application.SetFocus(form)
	if hasEsc {
		display.setShortcuts("F10:Confirm Esc:Cancel")
	} else {
		display.setShortcuts("F10:Confirm")
	}
}

func (display *Display) hideForm(name string, page tview.Primitive) {
	display.Pages.SendToBack(name)
	display.Pages.HidePage(name)
	display.Application.SetFocus(page)
	display.setShortcuts(display.ThisPage.GetKeyShortcuts())
	display.FormActive = false
}

func (display *Display) ChoosePasswordFormConfirm(password string) {
	key, params, err := security.GenerateMasterKeyArgon2(password)
	if err != nil {
		log.Infof("Failed to generate master key: %s", err)
		return
	}

	display.Config.Projects.Argon2Params = params

	err = config.SaveAppConfig(display.Config)
	if err != nil {
		log.Infof("Failed to save application config: %s", err)
		return
	}

	display.TokenKey = key

	display.hideForm("new-password-form", display.Index)
	display.loadProjects(false)
}

func (display *Display) PasswordFormConfirm(password string) error {
	key, err := security.ValidateMasterKeyArgon2(password, display.Config.Projects.Argon2Params)
	if err != nil {
		log.Infof("Failed to validate master key: %s", err)
		return err
	}

	display.TokenKey = key

	display.hideForm("password-form", display.Index)
	display.loadProjects(false)
	return nil
}

func (display *Display) SortFormConfirm(order SortFormOrder, reverse bool, sorter model.MergeReqSorter, saveDefault bool) {
	display.MergeReqs.Sorter = sorter
	display.MergeReqs.ReSort()
	display.Index.Refresh(display.Application, display.MergeReqs.Active)

	display.Config.Interface.MergeRequestSort = sortOrderToName(order, reverse)
	config.SaveAppConfig(display.Config)

	display.SortFormCancel()
}

func (display *Display) SortFormCancel() {
	display.hideForm("sort-form", display.Index)
}

func (display *Display) IndexPagePickSort() {
	log.Info("Show sort")
	display.showForm("sort-form", display.SortForm, true)
}

func (display *Display) FilterFormConfirm(filter model.MergeReqFilter) {
	display.MergeReqs.Filter = filter
	display.MergeReqs.ReFilter()
	display.Index.Refresh(display.Application, display.MergeReqs.Active)

	display.FilterFormCancel()
}

func (display *Display) FilterFormCancel() {
	display.hideForm("filter-form", display.Index)
	display.hideForm("quick-filter-form", display.Index)
}

func (display *Display) IndexPagePickFilter() {
	log.Info("Show filter")
	display.showForm("filter-form", display.FilterForm, true)
}

func (display *Display) IndexPagePickQuickFilter() {
	log.Info("Show quick filter")
	display.showForm("quick-filter-form", display.QuickFilterForm, true)
}

func (display *Display) ConfirmFormResult(confirmed, dontaskagain bool) {
	if dontaskagain {
		display.Config.Interface.DontConfirmExit = true
		config.SaveAppConfig(display.Config)
	}
	if confirmed {
		display.Stop()
	} else {
		display.hideForm("exit-confirm-form", display.Index)
		display.CtrlC = false
	}
}

func (display *Display) IndexPageQuit() {
	if display.Config.Interface.DontConfirmExit {
		display.Stop()
	} else {
		display.showForm("exit-confirm-form", display.ExitConfirmForm, true)
	}
}

func (display *Display) IndexPageViewMergeRequest(mreq model.MergeReq) {
	display.switchToPage(display.Overview)
	display.VersionForm.Refresh(display.Application, &mreq)
	display.Overview.Refresh(display.Application, &mreq)
	display.Engine.MarkRead(mreq)
}

func (display *Display) IndexPageViewProjects() {
	display.switchToPage(display.Projects)
}

func (display *Display) IndexPageRefreshMergeRequests() {
	display.Engine.RefreshRepos()
}

func (display *Display) OverviewPageQuit() {
	display.switchToPage(display.Index)
}

func (display *Display) OverviewPageRefreshMergeRequest(mreq *model.MergeReq) {
	display.Engine.RefreshMergeRequest(*mreq)
}

func (display *Display) OverviewPageAcceptMergeReq(mreq *model.MergeReq) {
	display.Engine.AcceptMergeRequest(*mreq)
}

func (display *Display) OverviewPageApproveMergeReq(mreq *model.MergeReq) {
	display.Engine.ApproveMergeRequest(*mreq)
}

func (display *Display) OverviewPageUnapproveMergeReq(mreq *model.MergeReq) {
	display.Engine.UnapproveMergeRequest(*mreq)
}

func (display *Display) OverviewPagePickVersion() {
	display.showForm("version-form", display.VersionForm, true)
}

func (display *Display) OverviewPageChangePatch(num int) {
	display.switchToPage(display.Detail)
	display.Detail.SelectPatch(num)
}

func (display *Display) DetailPageQuit() {
	display.switchToPage(display.Overview)
}

func (display *Display) DetailPageRefreshMergeRequest(mreq *model.MergeReq) {
	display.Engine.RefreshMergeRequest(*mreq)
}

func (display *Display) DetailPageAddMergeReqComment(mreq *model.MergeReq, text string, standAlone bool, context *model.CommentContext) {
	display.Engine.AddMergeRequestThread(*mreq, text, standAlone, context)
}

func (display *Display) DetailPageAddMergeReqReply(mreq *model.MergeReq, thread, text string) {
	display.Engine.AddMergeRequestReply(*mreq, thread, text)
}

func (display *Display) DetailPageResolveMergeReqThread(mreq *model.MergeReq, thread string, resolved bool) {
	display.Engine.ResolveMergeRequestThread(*mreq, thread, resolved)
}

func (display *Display) DetailPageLoadMergeReqSeriesPatches(mreq *model.MergeReq, series *model.Series) {
	display.Engine.LoadMergeRequestSeriesPatches(*mreq, *series)
}

func (display *Display) DetailPageLoadMergeReqCommitDiffs(mreq *model.MergeReq, commit *model.Commit) {
	display.Engine.LoadMergeRequestCommitDiffs(*mreq, *commit)
}

func (display *Display) DetailPageAcceptMergeReq(mreq *model.MergeReq) {
	display.Engine.AcceptMergeRequest(*mreq)
}

func (display *Display) DetailPageApproveMergeReq(mreq *model.MergeReq) {
	display.Engine.ApproveMergeRequest(*mreq)
}

func (display *Display) DetailPageUnapproveMergeReq(mreq *model.MergeReq) {
	display.Engine.UnapproveMergeRequest(*mreq)
}

func (display *Display) DetailPagePickVersion() {
	display.showForm("version-form", display.VersionForm, true)
}

func (display *Display) DetailPageChangePatch(num int) {
	display.Overview.SelectPatch(num)
}

func (display *Display) ProjectsPageQuit() {
	display.switchToPage(display.Index)
}

func (display *Display) ProjectsPageAddRepo() {
	log.Info("Showing add projects page")
	display.switchToPage(display.AddProject)
}

func (display *Display) ProjectsPageEditRepo(repo model.Repo) {
	log.Info("Showing add projects page")
	display.EditProject.SetRepo(repo)
	display.switchToPage(display.EditProject)
}

func (display *Display) AddProjectPageCancel() {
	display.switchToPage(display.Projects)
}

func (display *Display) EditProjectPageCancel() {
	display.switchToPage(display.Projects)
}

func (display *Display) AddProjectPageConfirm(repo model.Repo) {
	if repo.GlobalToken {
		for idx, _ := range display.Repos {
			thisrepo := &display.Repos[idx]
			if thisrepo.Server == repo.Server &&
				thisrepo.GlobalToken &&
				thisrepo.Token != repo.Token {
				thisrepo.Token = repo.Token
				display.Engine.UpdateRepository(*thisrepo)
			}
		}
	}

	display.Repos = append(display.Repos, repo)

	display.Projects.Refresh(display.Application, display.Repos)
	display.FilterForm.Refresh(display.Application, display.Repos)
	display.Engine.AddRepository(repo)

	err := config.SaveProjects(display.Repos, display.TokenKey)
	if err != nil {
		log.Infof("Failed to save project list: %s", err)
	}

	display.switchToPage(display.Index)
}

func (display *Display) EditProjectPageConfirm(repo model.Repo) {
	log.Infof("Edit project %s", repo.String())
	for idx, oldrepo := range display.Repos {
		if oldrepo.Equal(&repo) {
			if repo.State == oldrepo.State {
				if oldrepo.Token != repo.Token {
					display.Engine.UpdateRepository(repo)
				}
			} else {
				if repo.State == model.RepoStateActive {
					display.Engine.AddRepository(repo)
				} else if oldrepo.State == model.RepoStateActive {
					display.Engine.RemoveRepository(repo)
				}
			}
			log.Infof("Edit new state %s %d", repo.State, idx)
			display.Repos[idx] = repo
		}
	}

	if repo.GlobalToken {
		for idx, _ := range display.Repos {
			otherrepo := &display.Repos[idx]
			if !otherrepo.Equal(&repo) &&
				otherrepo.Server == repo.Server &&
				otherrepo.GlobalToken &&
				otherrepo.Token != repo.Token {
				log.Infof("Update token in project %s", otherrepo.String())
				otherrepo.Token = repo.Token
				display.Engine.UpdateRepository(*otherrepo)
			}
		}
	}

	if repo.State == model.RepoStateHidden {
		display.MergeReqs.PurgeRepo(&repo)
	}
	display.Index.Refresh(display.Application, display.MergeReqs.Active)

	display.Projects.Refresh(display.Application, display.Repos)
	display.FilterForm.Refresh(display.Application, display.Repos)

	err := config.SaveProjects(display.Repos, display.TokenKey)
	if err != nil {
		log.Infof("Failed to save project list: %s", err)
	}

	display.switchToPage(display.Projects)
}

func (display *Display) AddProjectPageAutoFillToken(server, project string) string {
	for _, repo := range display.Repos {
		if repo.Server == server && repo.GlobalToken {
			return repo.Token
		}
	}
	return ""
}

func (display *Display) VersionFormConfirm(version int) {
	display.hideForm("version-form", display.ThisPage)
	display.Overview.SwitchVersion(version)
	display.Detail.SwitchVersion(version)
}

func (display *Display) VersionFormCancel() {
	display.hideForm("version-form", display.ThisPage)
}

func (display *Display) SnakeGameFinished() {
	display.switchToPage(display.Index)
}

func (display *Display) PacmanGameFinished() {
	display.switchToPage(display.Index)
}

func (display *Display) Status(msg string) {
	display.Application.QueueUpdateDraw(func() {
		display.Messages.Info(msg)
	})
}

func (display *Display) MergeRequestNotify(mreq *model.MergeReq) {
	log.Infof("Received merge request %s", mreq.String())
	display.MergeReqs.Insert(mreq)

	log.Info("Updating index page")
	display.Index.Refresh(display.Application, display.MergeReqs.Active)

	log.Info("Updating detail page")
	thatmreq := display.Index.GetSelectedMergeRequest()
	if thatmreq != nil && thatmreq.Equal(mreq) {
		display.VersionForm.Refresh(display.Application, mreq)
		display.Overview.Refresh(display.Application, mreq)
		display.Detail.Refresh(display.Application, mreq)
	}
}

func (display *Display) RepoAdded(repo model.Repo) {
}

func (display *Display) RepoRemoved(repo model.Repo) {
}

func (display *Display) setShortcuts(shortcuts string) {
	formatted := fmt.Sprintf("[%s:%s]",
		GetStyleColorName(ELEMENT_SHORTCUTS_TEXT),
		GetStyleColorName(ELEMENT_SHORTCUTS_FILL)) +
		shortcuts +
		strings.Repeat(" ", 500)
	display.KeyShortcuts.SetText(formatted)
}

func (display *Display) switchToPage(page Page) {
	display.Pages.SendToFront(page.GetName())
	display.FormActive = false
	display.ThisPage = page
	display.ThisPage.Activate()
	display.setShortcuts(display.ThisPage.GetKeyShortcuts())
}

func (display *Display) Run() {
	display.Application.Run()
}
