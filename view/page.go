// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"github.com/gdamore/tcell/v2"
	"gitlab.com/bichon-project/tview"
)

type Page interface {
	tview.Primitive
	GetName() string
	GetKeyShortcuts() string
	HandleInput(event *tcell.EventKey) *tcell.EventKey
	Activate()
}
